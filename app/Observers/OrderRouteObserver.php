<?php

namespace App\Observers;

use App\Models\OrderRoute;

class OrderRouteObserver
{
    /**
     * Handle the OrderRoute "created" event.
     *
     * @param  OrderRoute  $orderRoute
     * @return void
     */
    public function created(OrderRoute $orderRoute)
    {
        dd('esh');
        $route = $orderRoute->route;
        $route->current_order_count++;
        $route->save();
    }

    /**
     * Handle the OrderRoute "updated" event.
     *
     * @param  OrderRoute  $orderRoute
     * @return void
     */
    public function updated(OrderRoute $orderRoute)
    {
        //
    }

    /**
     * Handle the OrderRoute "deleted" event.
     *
     * @param  OrderRoute  $orderRoute
     * @return void
     */
    public function deleted(OrderRoute $orderRoute)
    {
        //
    }

    /**
     * Handle the OrderRoute "restored" event.
     *
     * @param  OrderRoute  $orderRoute
     * @return void
     */
    public function restored(OrderRoute $orderRoute)
    {
        //
    }

    /**
     * Handle the OrderRoute "force deleted" event.
     *
     * @param  OrderRoute  $orderRoute
     * @return void
     */
    public function forceDeleted(OrderRoute $orderRoute)
    {
        //
    }
}
