<?php

namespace App\Observers;

use App\Models\Role;

class RoleObserver
{
    /**
     * Handle the Role "created" event.
     *
     * @param Role $role
     * @return void
     */
    public function created(Role $role)
    {
        $super = Role::findByName(Role::SUPERADMIN);
        if (!$super->hasChild($role->id)) {
            $super->assignChildRoles($role->id);
        }
    }

    /**
     * Handle the Role "updated" event.
     *
     * @param Role $role
     * @return void
     */
    public function updated(Role $role)
    {
        $super = Role::findByName(Role::SUPERADMIN);
        if (!$super->hasChild($role->id)) {
            $super->assignChildRoles($role->id);
        }
    }

    /**
     * Handle the Role "deleting" event.
     *
     * @param Role $role
     * @return void
     */
    public function deleting(Role $role)
    {

    }

    /**
     * Handle the Role "deleted" event.
     *
     * @param Role $role
     * @return void
     */
    public function deleted(Role $role)
    {

    }

    /**
     * Handle the Role "restored" event.
     *
     * @param Role $role
     * @return void
     */
    public function restored(Role $role)
    {
        //
    }

    /**
     * Handle the Role "force deleted" event.
     *
     * @param \App\Models\Role $role
     * @return void
     */
    public function forceDeleted(Role $role)
    {
        //
    }
}
