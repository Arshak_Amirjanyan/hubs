<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, HasRoles;

    public const DELETE_CONDITIONS = [
        //       "self" => ["company" => ["partner"]],
        "self" => [],
        "relations" => [
            "orders" => [["scope" => "byNotStatus", "value" => Order::ORDER_STATUES["not_submitted"]]]]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'company_id',
        'name',
        'email',
        'phone',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'pivot'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier(): mixed
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * @return Builder
     */
    public function getVisibleUsersByRole(): Builder
    {
        return User::select('users.*')->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->where("model_has_roles.model_type", self::class)
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->whereIn('roles.id', $this->roles()->first()->childRoles()->pluck("id"));
    }

    /**
     * @return Collection
     */
    public function getVisibleRoles(): Collection
    {
        return $this->roles()->first()->childRoles ?? collect([]);
    }

    /**
     * @return string
     */
    public function getRoleNameAttribute(): string
    {
        return $this->roles()->first()->name;
    }

    /**
     * @return Role
     */
    public function getRoleAttribute(): Role
    {
        return $this->roles()->first();
    }

    /**
     * @param $value
     * @return string
     */
    public function setPasswordAttribute($value): string
    {
        return $this->attributes["password"] = bcrypt($value);
    }

    /**
     * @return belongsToMany
     */
    public function hubs(): belongsToMany
    {
        return $this->belongsToMany(Hub::class, 'user_hub', 'user_id', 'hub_id');
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, "user_id");
    }

    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * @param Builder $query
     * @param int $id
     * @return Builder
     */
    public function scopeBySameCompany(Builder $query, int $id): Builder
    {
        return $query->where('company_id', $id);
    }
}
