<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Hub extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'latitude',
        'longitude',
        'location'
    ];


    /**
     * @return array
     */
    public function getLocationAttribute(): array
    {
        return [$this->attributes['latitude'], $this->attributes['longitude']];
    }

    public function setLocationAttribute(array $location)
    {
        $this->attributes['latitude'] = $location[0];
        $this->attributes['longitude'] = $location[1];
    }

    /**
     * @return belongsToMany
     */
    public function users(): belongsToMany
    {
        return $this->belongsToMany(User::class, 'user_hub', 'hub_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function routesFromHub(): BelongsToMany
    {
        return $this->belongsToMany(Hub::class, 'routes', 'source_id', "destination_id");
    }

    /**
     * @return BelongsToMany
     */
    public function routesToHub(): BelongsToMany
    {
        return $this->belongsToMany(Hub::class, 'routes', 'destination_id', "source_id");
    }

    public function routesFrom(): HasMany
    {
        return $this->hasMany(Route::class, 'source_id');
    }

    public function routesTo(): HasMany
    {
        return $this->hasMany(Route::class, 'destination_id');
    }

    public function scopeFindClosestHub(Builder $query, array $location, int $count)
    {
        return $query->select(["*", $query->raw("6371 * asin(sqrt( pow(sin((`latitude` * pi()/180 - $location[0] * pi() / 180)/2),2)
                        +
                        cos(( `latitude` * pi() / 180)) * cos(( $location[0] * pi() /180 ))
                        *
                        pow(sin((( `longitude` * pi() / 180)-$location[1] * pi()/180)/2), 2))) as distance")])->from("hubs")->orderBy("distance")->limit($count);
    }
}
