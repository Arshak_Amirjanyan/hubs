<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    use HasFactory;

    /**
     * possible delivery statuses for order
     */
    public const DELIVERY_STATUSES = [
        "in_hub" => "in_hub",
        "en_route" => "en_route",
        "delivered" => "delivered"
    ];

    /**
     * possible pickup and delivery
     * destination types
     */
    public const DELIVERY_TYPES = [
        "hub" => "hub",
        "locker" => "locker",
        "home" => "home"
    ];

    /**
     * possible statuses of order
     */
    public const ORDER_STATUES = [
        "not_submitted" => "not_submitted",
        "submitted" => "submitted",
        "approved" => "approved"
    ];

    public const DELETE_CONDITIONS = [
        "self" =>
            ["order_status" => ["not_submitted"]],
        "relations" => []
        //   ["users" => [["scope" => "byOrderStatus", "value" => "not_submitted"]]]
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'source_id',
        'destination_id',
        "current_location_id",
        'start_location',
        'end_location',
        'tracking_code',
        'order_status',
        'delivery_status',
        'pickup_type',
        'delivery_type'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        "start_location" => 'array',
        "end_location" => 'array'
    ];

    /**
     * @return BelongsToMany
     */
    public function routes(): BelongsToMany
    {
        return $this->belongsToMany(Route::class, "order_routes", 'order_id', 'route_id');
    }

    /**
     * @return BelongsToMany
     */
    public function parcels(): BelongsToMany
    {
        return $this->belongsToMany(Parcel::class, "order_parcel", "order_id", "parcel_id");
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }


    /**
     * @return BelongsTo
     */
    public function sourceHub(): BelongsTo
    {
        return $this->belongsTo(Hub::class, "source_id");
    }

    /**
     * @return BelongsTo
     */
    public function destinationHub(): BelongsTo
    {
        return $this->BelongsTo(Hub::class, "destination_id");
    }

    /**
     * used as a filter in OrderRepository
     *
     * @param Builder $query
     * @param $hubs
     * @return Builder
     */
    public function scopeByHubs(Builder $query, $hubs): Builder
    {

        return $query->select("orders.*")
            ->join("order_routes", 'order_routes.order_id', '=', 'orders.id')
            ->join("routes", "order_routes.route_id", "routes.id")
            ->whereIn("source_id", $hubs)
            ->orWhereIn("destination_id", $hubs);
    }

    /**
     * Used as a filter in OrderRepository
     *
     * @param Builder $query
     * @param int $user_id
     * @return Builder
     */
    public function scopeByUser(Builder $query, int $user_id): Builder
    {
        return $query->where("user_id", $user_id);
    }

    /**
     * @param Builder $query
     * @param $ids
     * @return Builder
     */
    public function scopeByCompanyUsers(Builder $query, $ids): Builder
    {
        return $query->whereIn("user_id", $ids);
    }

    /**
     * @param Builder $query
     * @param string $status
     * @return Builder
     */
    public function scopeByOrderStatus(Builder $query, string $status): Builder
    {
        return $query->where("order_status", $status);
    }

    /**
     * @param Builder $query
     * @param string $status
     * @return Builder
     */
    public function scopeByDeliveryStatus(Builder $query, string $status): Builder
    {
        return $query->where("delivery_status", $status);
    }

    /**
     * @param Builder $query
     * @param string $type
     * @return Builder
     */
    public function scopeByPickupType(Builder $query, string $type): Builder
    {
        return $query->where("pickup_type", $type);
    }

    /**
     * @param Builder $query
     * @param string $type
     * @return Builder
     */
    public function scopeByDeliveryType(Builder $query, string $type): Builder
    {
        return $query->where("delivery_type", $type);
    }

    /**
     * @param Builder $query
     * @param string $status
     * @return Builder
     */
    public function scopeByNotStatus(Builder $query, string $status): Builder
    {
        return $query->where("order_status", "!=", $status);
    }
}
