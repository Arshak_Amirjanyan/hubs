<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Route extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        "source_id",
        "destination_id",
        "current_order_count"
    ];


    /**
     * @return BelongsTo
     */
    public function sourceHub(): BelongsTo
    {
        return $this->belongsTo(Hub::class, "source_id");
    }

    /**
     * @return BelongsTo
     */
    public function destinationHub(): BelongsTo
    {
        return $this->belongsTo(Hub::class, "destination_id");
    }

    /**
     * @param Builder $query
     * @param $ids
     * @return Builder
     */
    public function scopeByHubs(Builder $query, $ids): Builder
    {
        return $query->whereIn("source_id", $ids)->orWhereIn("destination_id", $ids);
    }

}
