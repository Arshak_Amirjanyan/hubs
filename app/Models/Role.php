<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends \Spatie\Permission\Models\Role
{
    use HasFactory;

    public const SUPERADMIN = 'superadmin';
    public const PARTNER = 'partner';
    public const MANAGER = 'manager';
    public const COURIER = 'courier';
    public const USER = 'user';

    /**
     * @return BelongsToMany
     */
    public function childRoles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'assignable_roles', 'role_id', 'assignee_id');
    }

    /**
     * @return BelongsToMany
     */
    public function parentRoles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'assignable_roles', 'assignee_id', 'role_id');
    }


    /**
     * @param ...$ids
     * @return $this
     */
    public function assignChildRoles($ids, bool $deleteOthers = false): Role
    {
        $this->childRoles()->sync($ids, $deleteOthers);
        return $this;
    }

    /**
     * @param ...$ids
     * @return $this
     */
    public function assignParentRoles($ids, bool $deleteOthers = false): Role
    {
        $this->parentRoles()->sync($ids, $deleteOthers);
        return $this;
    }

    /**
     * @param $id
     * @return bool
     */
    public function hasChild($id): bool
    {
        return $this->childRoles->contains($id);
    }

    /**
     * @param $id
     * @return bool
     */
    public function hasParent($id): bool
    {
        return $this->parentRoles->contains($id);
    }

}
