<?php

namespace App\Models;

abstract class Model extends \Illuminate\Database\Eloquent\Model
{
    public const DELETE_CONDITIONS = [
        "self" => [],
        "relations" => []
    ];
}
