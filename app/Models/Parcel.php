<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Parcel extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        "route_id",
        "tracking_code"
    ];

    /**
     * @return BelongsToMany
     */
    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class, "order_parcel", "parcel_id", "order_id");
    }

    /**
     * @return BelongsTo
     */
    public function route(): BelongsTo
    {
        return $this->belongsTo(Route::class, "route_id");
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }

    /**
     * filter parcels by hubs(parcel policy viewAny)
     *
     * @param Builder $query
     * @param $ids
     * @return Builder
     */
    public function scopeHubs(Builder $query, $ids): Builder
    {
        return $query->select("parcels.*")
            ->join("routes", "parcels.route_id", "routes.id")
            ->whereIn("routes.source_id", $ids)
            ->orWhereIn("routes.destination_id", $ids);
    }
}
