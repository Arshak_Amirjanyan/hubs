<?php

namespace App\Providers;

use App\Listeners\RoutesEventSubscriber;
use App\Models\Order;
use App\Models\OrderRoute;
use App\Models\User;
use App\Observers\OrderObserver;
use App\Observers\OrderRouteObserver;
use App\Observers\RoleObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Models\Role;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Role::observe(RoleObserver::class);
        Order::observe(OrderObserver::class);
        OrderRoute::observe(OrderRouteObserver::class);
    }

    protected $subscribe = [
        RoutesEventSubscriber::class,
    ];
}
