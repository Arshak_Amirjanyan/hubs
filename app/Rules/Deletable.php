<?php

namespace App\Rules;

use App\Repositories\Repository;
use Illuminate\Contracts\Validation\Rule;

class Deletable implements Rule
{
    /**
     * @var Repository
     */
    protected Repository $repository;

    /**
     * @var string
     */
    protected string $failedCondition;

    /**
     * @var string
     */
    protected string $failedRelation;

    /**
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $this->failedCondition = $this->repository->checkDeleteAbilityForModel($value);
        $this->failedRelation = $this->repository->checkDependableRelationsForDeleting($value);
        return !(bool)$this->failedRelation && !(bool)$this->failedCondition;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return $this->failedCondition ? 'This :attribute can not be deleted because of ' . preg_split('/(?=[A-Z])/', $this->failedCondition)[0] . ' value' :
            'This :attribute has ' . preg_split('/(?=[A-Z])/', $this->failedRelation)[0] . ' delete them first';
    }
}
