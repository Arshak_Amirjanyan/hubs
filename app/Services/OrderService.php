<?php

namespace App\Services;

use App\Models\Hub;
use App\Models\Order;
use App\Models\OrderRoute;
use App\Repositories\OrderRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class OrderService
{

    /**
     * @var Repository
     */
    protected Repository $orderRepository;

    /**
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function filterPartnerOrders(array $filters): array
    {
        if (!array_key_exists("user", $filters)) {
            $filters["companyUsers"] = Auth::user()->company->users->pluck('id');
        }

        return $filters;
    }

    /**
     * @param array $data
     * @return array
     */
    public function preparePartnerCreationData(array $data): array
    {
        $data = $this->prepareDeliveryData($data);
        $data['user_id'] = Auth::user()->id;
        $data['order_status'] = Order::ORDER_STATUES["not_submitted"];

        return $data;
    }

    /**
     * @param array $data
     * @return array
     *
     */
    public function prepareOwnerCreationData(array $data): array
    {
        $data = $this->prepareDeliveryData($data);

        return $data;
    }

    /**
     * @param array $data
     * @return array
     */
    public function prepareDeliveryData(array $data): array
    {
        $sourceHub = Hub::findClosestHub($data["start_location"], 1)->first();
        $data["source_id"] = $sourceHub->id;
        $data["pickup_type"] = $data["start_location"] === $sourceHub->location ? Order::DELIVERY_TYPES["hub"] : Order::DELIVERY_TYPES["home"];
        $destinationHub = Hub::findClosestHub($data["end_location"], 1)->first();
        $data["destination_id"] = $destinationHub->id;
        $data["delivery_type"] = $data["end_location"] === $sourceHub->location ? Order::DELIVERY_TYPES["hub"] : Order::DELIVERY_TYPES["home"];

        return $data;
    }

    /**
     * @param Order $order
     * @return string
     */
    public function generateTrackingCode(Order $order): string
    {
        return "u" . $order->user->id . "c" . $order->user->company->id . "t" . (string)microtime(true) * 10000;
    }

    /**
     * @param Order $order
     * @param int $hub_id
     * @return void
     */
    public function updateRouteStatus(Order $order, int $hub_id)
    {
        if ($route = $this->orderRepository->getCompletedRoute($order, $hub_id)) {
            $order->routes()->updateExistingPivot($route->id, ["status" => "complete"]);
        }
    }


}
