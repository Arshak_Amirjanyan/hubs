<?php

namespace App\Services;

use App\Models\Order;

class DeliveryService
{
    /**
     * @param string $value
     * @return string
     */
    public function getNextDeliveryStatus(string $value = ""): string
    {
        return match ($value) {
            Order::DELIVERY_STATUSES["in_hub"] => Order::DELIVERY_STATUSES["en_route"],
            default => Order::DELIVERY_STATUSES["in_hub"],
        };
    }
}
