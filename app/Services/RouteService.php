<?php

namespace App\Services;

use App\Models\Route;
use Illuminate\Support\Collection;

class RouteService
{
    /**
     * @param $source
     * @param $dest
     * @param $routeAdjList
     * @return Collection
     */
    public function generateRoutes($source, $dest, $routeAdjList): Collection
    {
        $successfulRoutes = collect([]);
        $visited = [];
        $path = ["cost" => 0, "route" => []];

        $this->checkAllPaths($source, $dest, $routeAdjList, $visited, $path, $successfulRoutes);

        return $successfulRoutes->sortBy("cost");
    }

    /**
     * @param $source
     * @param $dest
     * @param $routeAdjList
     * @param $visited
     * @param $path
     * @param $successfulRoutes
     * @return void
     */
    protected function checkAllPaths($source, $dest, $routeAdjList, $visited, $path, &$successfulRoutes): void
    {
        if ($source->id == $dest->id) {
            $successfulRoutes[] = $path;
        }

        $visited[$source->id] = true;

        foreach ($routeAdjList[$source->id] as $nextRoute) {
            if (!array_key_exists($nextRoute->destinationHub->id, $visited) || !$visited[$nextRoute->destinationHub->id]) {
                $path["route"][] = $nextRoute;
                $path["cost"] = $path["cost"] + $this->calculateCost($nextRoute);
                $this->checkAllPaths($nextRoute->destinationHub, $dest, $routeAdjList, $visited, $path, $successfulRoutes);
                array_pop($path);
                $path["cost"] = $path["cost"] - $this->calculateCost($nextRoute);
            }
        }

        $visited[$source->id] = false;
    }

    /**
     * @param $route
     * @return int
     */
    public function calculateCost(Route $route): int
    {
        if ($route->current_order_count > 0) {
            return 1;
        }
        return 2;
    }
}
