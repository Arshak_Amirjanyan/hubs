<?php

namespace App\Policies;

use App\Models\User;
use App\Models\order;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Http\FormRequest;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->can("order-full")) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param FormRequest $request
     * @return bool
     */
    public function viewAny(User $user, FormRequest $request): bool
    {
        $request->merge(["hubs" => $user->hubs->pluck('id')]);

        return $user->can('order-list');
    }

    /**
     * @param User $user
     * @param order $order
     * @return bool
     */
    public function view(User $user, order $order): bool
    {
        return $user->can('order-view') && Order::byHubs($user)->get()->contains($order);
    }

    /**
     * @param User $user
     * @param order $order
     * @return bool
     */
    public function viewOwn(User $user, Order $order): bool
    {
        return $user->can('order-view') && ($user->id === $order->user_id || $this->checkUserAuthority($user, $order));
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can('order-create');
    }

    /**
     * @param User $user
     * @param order $order
     * @return bool
     */
    public function update(User $user, order $order): bool
    {
        return $user->can('order-edit');
    }

    public function updateOwn(User $user, order $order): bool
    {
        return $user->can('order-edit') && $user->id === $order->user_id || $this->checkUserAuthority($user, $order);
    }

    /**
     * @param User $user
     * @param order $order
     * @return bool
     */
    public function delete(User $user, order $order): bool
    {
        return $user->can('order-delete') && ($user->id === $order->user_id || $this->checkUserAuthority($user, $order));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\order $order
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, order $order)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\order $order
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, order $order)
    {

    }

    /**
     * @param User $user
     * @return bool
     */
    public function generateRoute(User $user): bool
    {
        return $user->can("route-generate");
    }

    /**
     * @param User $user
     * @return bool
     */
    public function assignRoute(User $user): bool
    {
        return $user->can("route-assign");
    }

    /**
     * @param User $user
     * @param User $customer
     * @return bool
     */
    public function checkUserAuthority(User $user, Order $order): bool
    {
        return $user->getVisibleUsersByRole()->sameCompany($user->company_id)->get("id")->contains($order->user_id);
    }
}
