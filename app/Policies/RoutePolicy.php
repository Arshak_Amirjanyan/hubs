<?php

namespace App\Policies;

use App\Models\Route;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoutePolicy
{
    use HandlesAuthorization;


    /**
     * @param User $user
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->can("route-full")) {
            return true;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->can("route-list");
    }

    /**
     * @param User $user
     * @param Route $route
     * @return bool
     */
    public function view(User $user, Route $route): bool
    {
        return $user->can("route-view");
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can("route-create");
    }

    /**
     * @param User $user
     * @param Route $route
     * @return bool
     */
    public function update(User $user, Route $route)
    {
        return $user->can('route-update');
    }

    /**
     * @param User $user
     * @param Route $route
     * @return bool
     */
    public function delete(User $user, Route $route): bool
    {
        return $user->can("route-delete");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Route $route
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Route $route)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Route $route
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Route $route)
    {
        //
    }
}
