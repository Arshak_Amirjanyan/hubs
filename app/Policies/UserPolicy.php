<?php

namespace App\Policies;

use App\Http\Requests\Owner\User\CreateUserRequest;
use App\Http\Requests\Owner\User\UpdateUserRequest;
use App\Models\Company;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->can("user-full")) {
            return true;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->can('user-list');
    }

    /**
     * @param User $user
     * @param User $model
     * @return bool
     */
    public function view(User $user, User $model): bool
    {
        return ($user->can('user-view') && $this->checkRoleAbilityForModel($user, $model))
            || $user->id === $model->id;
    }

    /**
     * @param User $user
     * @param FormRequest $request
     * @return bool
     */
    public function create(User $user, FormRequest $request): bool
    {
        return $user->can('user-create') && $this->checkRoleAbilityForRequest($user, $request);
    }

    /**
     * @param User $user
     * @param User $model
     * @param FormRequest $request
     * @return bool
     */
    public function update(User $user, User $model, FormRequest $request): bool
    {
        $authorized = $user->can("user-edit") && $this->checkRoleAbilityForModel($user, $model);

        if ($authorized && $request->get('role_id')) {
            $authorized = $authorized && $this->checkRoleAbilityForRequest($user, $request);
        }

        return $authorized;
    }

    /**
     * @param User $user
     * @param User $model
     * @return bool
     */
    public function delete(User $user, User $model): bool
    {
        return $user->can("user-delete") && $this->checkRoleAbilityForModel($user, $model);
    }

    /**
     * @param User $user
     * @param User $model
     * @return bool
     */
    public function restore(User $user, User $model): bool
    {
        //
    }

    /**
     * @param User $user
     * @param User $model
     * @return bool
     */
    public function forceDelete(User $user, User $model): bool
    {
        //
    }

    /**
     * @param User $user
     * @param User $model
     * @return bool
     */
    public function listUserPermissions(User $user, User $model): bool
    {
        return $user->id === $model->id || ($user->can("permissions-list") && $this->checkRoleAbilityForModel($user, $model));
    }

    /**
     * @param User $user
     * @param User $model
     * @return bool
     */
    public function updateUserPermissions(User $user, User $model): bool
    {
        return $user->can("permissions-update") && $this->checkRoleAbilityForModel($user, $model);
    }

    /**
     * @param User $user
     * @param $model
     * @return bool
     */
    public function checkRoleAbilityForModel(User $user, $model): bool
    {
        return $user->getVisibleRoles()->contains($model->roles()->first());
    }

    /**
     * @param User $user
     * @param Request $request
     * @return bool
     */
    public function checkRoleAbilityForRequest(User $user, Request $request): bool
    {
        return $user->getVisibleRoles()->contains($request->role_id);
    }
}
