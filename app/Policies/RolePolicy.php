<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Http\FormRequest;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->can("role-list");
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function view(User $user, Role $role): bool
    {
        return ($user->can("role-view") && $user->roles()->first()->hasChild($role)) || $user->roles()->first()->id === $role->id;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->can('role-create');
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function update(User $user, Role $role): bool
    {
        return $user->can("role-edit");
    }

    /**
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function delete(User $user, Role $role): bool
    {
        return $user->can("role-delete");
    }

    /**
     * @param User $user
     * @param Role $role
     * @return void
     */
    public function restore(User $user, Role $role)
    {
        //
    }

    /**
     * @param User $user
     * @param Role $role
     * @return void
     */
    public function forceDelete(User $user, Role $role)
    {
        //
    }
}
