<?php

namespace App\Policies;

use App\Models\Parcel;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Foundation\Http\FormRequest;

class ParcelPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->can("parcel-full")) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param FormRequest $request
     * @return bool
     */
    public function viewAny(User $user, FormRequest $request): bool
    {
        $request->merge(["hubs" => $user->hubs->pluck('id')]);

        return $user->can('parcel-list');
    }

    /**
     * @param User $user
     * @param Parcel $parcel
     * @return bool
     */
    public function view(User $user, Parcel $parcel): bool
    {
        return $user->can('parcel-view') && $this->authorizeUserCanSeeParcel($user, $parcel);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
       return $user->can('parcel-create');
    }

    /**
     * @param User $user
     * @param Parcel $parcel
     * @return bool
     */
    public function update(User $user, Parcel $parcel): bool
    {
        return $user->can("parcel-edit") && $this->authorizeUserCanSeeParcel($user, $parcel);
    }

    /**
     * @param User $user
     * @param Parcel $parcel
     * @return bool
     */
    public function delete(User $user, Parcel $parcel): bool
    {
        return $user->can("parcel-delete") && $this->authorizeUserCanSeeParcel($user, $parcel);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Parcel $parcel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Parcel $parcel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Parcel $parcel
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Parcel $parcel)
    {
        //
    }

    /**
     * @param User $user
     * @param Parcel $parcel
     * @return bool
     */
    public function authorizeUserCanSeeParcel(User $user, Parcel $parcel): bool
    {
        return Parcel::Hubs($user->hubs->pluck('id'))->get()->contains($parcel) || $parcel->user_id === $user->id;
    }
}
