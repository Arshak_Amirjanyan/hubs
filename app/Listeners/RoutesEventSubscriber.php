<?php

namespace App\Listeners;

use App\Events\RouteAttached;
use App\Events\RouteDetached;
use App\Events\RouteSynced;
use App\Models\Route;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class RoutesEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handleRouteSync($event)
    {
        Route::whereIn("id", $event->changes["attached"])->update(["current_order_count" => DB::raw("current_order_count + 1")]);
        Route::whereIn("id", $event->changes["detached"])->update(["current_order_count" => DB::raw("current_order_count - 1")]);
    }

    public function subscribe($events)
    {
        $events->listen(
            RouteSynced::class,
            [RoutesEventSubscriber::class, 'handleRouteSync']
        );
    }
}
