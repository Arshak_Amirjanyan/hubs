<?php

namespace App\Repositories;

use App\Models\Parcel;

class ParcelRepository extends Repository
{
    /**
     * @param Parcel $model
     */
    public function __construct(Parcel $model)
    {
        parent::__construct($model);
    }

    public function addOrders($parcel, $order_ids)
    {
        $parcel->orders()->attach($order_ids);
    }
}
