<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class OrderRepository extends Repository
{
    /**
     * @param Order $model
     */
    public function __construct(Order $model)
    {
        parent::__construct($model);
    }

    public function getCompletedRoute(Order $order, int $hub_id)
    {
        return $order->routes()->where("destination_id", $hub_id)->first() ?? null;
    }
}
