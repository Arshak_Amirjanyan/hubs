<?php

namespace App\Repositories;

use App\Models\Company;

class CompanyRepository extends Repository
{
    /**
     * @param Company $model
     */
    public function __construct(Company $model)
    {
        parent::__construct($model);
    }
}
