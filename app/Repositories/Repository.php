<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Model;

abstract class Repository
{
    /**
     * @var Model|User
     */
    protected Model|User $model;

    /**
     * @param Model|User $model
     */
    public function __construct(Model|User $model)
    {
        $this->model = $model;
    }

    public function applyFilters(array $filters = []): Builder
    {
        $query = $this->model->newQuery();
        foreach ($filters as $filter => $value) {
            $filter = $this->beautifyFilter($filter);
            $query->mergeConstraintsFrom($this->model::$filter($value));
        }

        return $query;
    }

    public function beautifyFilter($filter): string
    {
        $filter = "by" . $filter;
        $filter = str_replace('_', '', $filter);

        return $filter;
    }

    /**
     * @param Model|User $model
     * @return string
     */
    public function checkDependableRelationsForDeleting(Model|User $model): string
    {
        $failedRelation = "";
        foreach ($model::DELETE_CONDITIONS["relations"] as $rel => $constraints) {
            $query = $model->$rel();
            foreach ($constraints as $constraint) {
                $scope = $constraint["scope"];
                $value = $constraint["value"];
                $query = $query->$scope($value);
            }
            if ($query->first()) {
                $failedRelation = $rel;
                break;
            }
        }

        return $failedRelation;
    }

    /**
     * @param Model|User $model
     * @return string
     */
    public function checkDeleteAbilityForModel(Model|User $model): string
    {
        $failedCondition = "";
        foreach ($model::DELETE_CONDITIONS["self"] as $column => $values) {
            if (!in_array($model->$column, $values)) {
                $failedCondition = $column;
                break;
            };
        }
        return $failedCondition;
    }
}
