<?php

namespace App\Repositories;

use App\Models\Hub;
use JetBrains\PhpStorm\Pure;

class HubRepository extends Repository
{
    /**
     * @param Hub $model
     */
    public function __construct(Hub $model)
    {
        parent::__construct($model);
    }
}
