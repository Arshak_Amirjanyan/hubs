<?php

namespace App\Repositories;

use App\Models\Route;

class RouteRepository extends Repository
{
    public function __construct(Route $model)
    {
        parent::__construct($model);
    }
}
