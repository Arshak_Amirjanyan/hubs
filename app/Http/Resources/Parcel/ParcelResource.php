<?php

namespace App\Http\Resources\Parcel;

use App\Http\Resources\Order\OrdersCollection;
use App\Http\Resources\Route\RouteResource;
use App\Models\Route;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class ParcelResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|Collection|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return [
            "tracking_code" => $this->tracking_code,
            "route" => RouteResource::make($this->route),
            "orders" => $this->whenLoaded("orders", function () {
                return OrdersCollection::make($this->orders);
            })
        ];
    }
}
