<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Company\CompanyResource;
use App\Http\Resources\Permission\PermissionsCollection;
use App\Http\Resources\Role\RoleResource;
use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use \JsonSerializable;
use \Illuminate\Http\Request;

class UserResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|Collection|JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "phone" => $this->phone,
            "company" => CompanyResource::make($this->company),
            "role" => $this->when((Auth::check() && Auth::user()->can('role-view') && $this->roles->first()), function () {
                return RoleResource::make($this->roles()->first());
            }),
            "permissions" => $this->when($this->relationLoaded('permissions'), PermissionsCollection::make($this->getAllPermissions())),
        ];
    }
}
