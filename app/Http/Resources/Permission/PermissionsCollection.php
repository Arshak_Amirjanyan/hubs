<?php

namespace App\Http\Resources\Permission;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;
use JsonSerializable;
use \Illuminate\Http\Request;

class PermissionsCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public $collects = PermissionResource::class;

    /**
     * @param $request
     * @return array|Arrayable|Collection|JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return $this->collection;
    }
}
