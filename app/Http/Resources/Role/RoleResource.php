<?php

namespace App\Http\Resources\Role;

use App\Http\Resources\Permission\PermissionsCollection;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use JsonSerializable;

class RoleResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|Collection|JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "permissions" => $this->when($this->relationLoaded('permissions'), PermissionsCollection::make($this->permissions)),
            "childRoles" => $this->whenLoaded('childRoles',
                RolesCollection::make($this->childRoles()
                    ->without(['permissions', 'childRoles', 'parentRoles'])->get())),
            "parentRoles" => $this->whenLoaded('parentRoles',
                RolesCollection::make($this->parentRoles()
                    ->without(['permissions', 'childRoles', 'parentRoles'])->get()))
        ];
    }
}
