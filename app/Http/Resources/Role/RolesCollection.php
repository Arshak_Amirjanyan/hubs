<?php

namespace App\Http\Resources\Role;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class RolesCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public $collects = RoleResource::class;

    /**
     * @param $request
     * @return array|Arrayable|Collection|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return $this->collection;
    }
}
