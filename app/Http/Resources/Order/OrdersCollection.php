<?php

namespace App\Http\Resources\Order;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class OrdersCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public $collects = OrderResource::class;

    /**
     * @param $request
     * @return array|Arrayable|Collection|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return $this->collection;
    }
}
