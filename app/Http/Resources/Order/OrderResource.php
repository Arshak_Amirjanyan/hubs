<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Hub\HubResource;
use App\Http\Resources\Route\RouteResource;
use App\Http\Resources\Route\RoutesCollection;
use App\Http\Resources\User\UserResource;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\RouteCollection;
use Illuminate\Support\Collection;

class OrderResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|Collection|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return [
            "id" => $this->id,
            "tracking_code" => $this->tracking_code,
            "user" => $this->whenLoaded('user', function () {
                return UserResource::make($this->user);
            }),
            "start_location" => $this->start_location,
            "end_location" => $this->end_location,
            "source_hub" => $this->whenLoaded("sourceHub", function () {
                return HubResource::make($this->sourceHub);
            }),
            "destination_hub" => $this->whenLoaded("destinationHub", function () {
                return HubResource::make($this->destinationHub);
            }),
            "pickup_type" => $this->pickup_type,
            "delivery_type" => $this->delivery_type,
            "delivery_status" => $this->delivery_status,
            "order_status" => $this->order_status,
            "route" => $this->whenLoaded("routes", function () {
                return RoutesCollection::make($this->routes);
            })
        ];
    }
}
