<?php

namespace App\Http\Resources\Route;

use App\Http\Resources\Hub\HubResource;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;

class RouteResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|\JsonSerializable
    {
        return [
            "id" => $this->id,
            "current_order_count" => $this->current_order_count,
            "source_hub" => HubResource::make($this->sourceHub),
            "destination_hub" => HubResource::make($this->destinationHub)
        ];
    }
}
