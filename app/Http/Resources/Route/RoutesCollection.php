<?php

namespace App\Http\Resources\Route;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RoutesCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public $collects = RouteResource::class;

    /**
     * @param $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|\JsonSerializable
    {
        return $this->collection;
    }
}
