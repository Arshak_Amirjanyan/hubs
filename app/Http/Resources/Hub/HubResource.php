<?php

namespace App\Http\Resources\Hub;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class HubResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|Collection|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'location' => $this->location
        ];
    }
}
