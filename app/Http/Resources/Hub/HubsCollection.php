<?php

namespace App\Http\Resources\Hub;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class HubsCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public $collects = HubResource::class;

    /**
     * @param $request
     * @return array|Arrayable|Collection|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|Collection|\JsonSerializable
    {
        return $this->collection;
    }
}
