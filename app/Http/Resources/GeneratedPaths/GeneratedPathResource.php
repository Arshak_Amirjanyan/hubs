<?php

namespace App\Http\Resources\GeneratedPaths;

use App\Http\Resources\Hub\HubResource;
use App\Http\Resources\Route\RouteResource;
use App\Http\Resources\Route\RoutesCollection;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\RouteCollection;

class GeneratedPathResource extends JsonResource
{
    /**
     * @param $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|\JsonSerializable
    {
        return [
            "cost" => $this["cost"],
            "route" => RoutesCollection::make($this["route"]),
        ];
    }
}
