<?php

namespace App\Http\Resources\GeneratedPaths;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Resources\Json\ResourceCollection;

class GeneratedPathsCollection extends ResourceCollection
{
    /**
     * @var string
     */
    public $collects = GeneratedPathResource::class;

    /**
     * @param $request
     * @return array|Arrayable|\JsonSerializable
     */
    public function toArray($request): array|Arrayable|\JsonSerializable
    {
        return $this->collection;
    }
}
