<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' =>'required|digits_between:8,12|numeric',
            'password' => 'required|min:6'
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'name.required' => 'A name is required',
            'email.required' => 'An email is required',
            'email.unique' => 'The email is already taken',
            'phone.required' => 'phone number is required',
            'phone.min:8' => 'phone number must be at least 8 characters long',
            'phone.numeric' => 'phone number cant contain characters other than 0-9',
            'password.required' => 'The password is required',
            'password.min:6' => 'The password must be at least 6 characters long'
        ];
    }
}
