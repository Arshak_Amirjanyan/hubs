<?php

namespace App\Http\Requests;

use App\Repositories\Repository;
use Illuminate\Foundation\Http\FormRequest;

abstract class DeleteModelRequest extends FormRequest
{
    /**
     * @var Repository
     */
    protected Repository $repository;

    /**
     * @param Repository $repository
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param $content
     */
    public function __construct(Repository $repository, array $query = [], array $request = [],
                                array $attributes = [], array $cookies = [], array $files = [],
                                array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->repository = $repository;
    }

    /**
     * @param $keys
     * @return array
     */
    public function all($keys = null): array
    {
        return array_merge(parent::all(), $this->route()->parameters());
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
