<?php

namespace App\Http\Requests\Partner\User;

use App\Http\Requests\DeleteModelRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class DeleteUserRequest extends DeleteModelRequest
{
    /**
     * @param UserRepository $repository
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param $content
     */
    public function __construct(UserRepository $repository, array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($repository, $query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('delete', $this->user) && Auth::user()->company_id === $this->user->company_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
