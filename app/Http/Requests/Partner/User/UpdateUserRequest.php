<?php

namespace App\Http\Requests\Partner\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('update', [$this->user, $this])
            && Auth::user()->company_id === $this->user->company_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'role_id' => 'required|exists:roles,id',
            'email' => 'required|unique:users,email,'.$this->user->id,
            'phone' =>'required|digits_between:8,12|numeric|unique:users,phone,'.$this->user->id,
            'password' => 'required|min:6'
        ];
    }
}
