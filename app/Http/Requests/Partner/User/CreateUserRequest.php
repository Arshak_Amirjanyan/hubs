<?php

namespace App\Http\Requests\Partner\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('create', [User::class, $this]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'role_id' => 'required|exists:roles,id|in:'.implode("," , Auth::user()->roles()->first()->childRoles->pluck("id")->toArray()),
            'email' => 'required|unique:users,email',
            'phone' =>'required|digits_between:8,12|numeric|unique:users,phone',
            'password' => 'required|min:6'
        ];
    }
}
