<?php

namespace App\Http\Requests\Partner\Order;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can("create", Order::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "tracking_code" => "required",
            "start_location" => "array|required",
            "start_location.*" => "numeric|required",
            "end_location" => "array|required",
            "end_location.*" => "numeric|required",
        ];
    }
}
