<?php

namespace App\Http\Requests\Partner\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can('updateOwn', $this->order);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "start_location" => "array|required",
            "start_location.*" => "numeric|required",
            "end_location" => "array|required",
            "end_location.*" => "numeric|required",
        ];
    }
}
