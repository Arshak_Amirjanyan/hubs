<?php

namespace App\Http\Requests\Owner\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('role-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "name" => "required|unique:roles",
            "permissions" => "required|array",
            'child_roles' => "required|array",
            'child_roles.*' => "numeric|exists:roles,id",
            'parent_roles' => "required|array",
            'parent_roles.*' => "numeric|exists:roles,id",
            "permissions.*" => "numeric|exists:permissions,id"
        ];
    }
}
