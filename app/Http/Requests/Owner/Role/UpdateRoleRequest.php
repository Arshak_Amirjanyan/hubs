<?php

namespace App\Http\Requests\Owner\Role;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('role-edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "name" => "required|unique:roles,name,".$this->role->id,
            'child_roles' => "required|array",
            'child_roles.*' => "numeric|exists:roles,id",
            'parent_roles' => "required|array",
            'parent_roles.*' => "numeric|exists:roles,id",
            "permissions" => "required|array",
            "permissions.*" => "numeric|exists:permissions,id"
        ];
    }
}
