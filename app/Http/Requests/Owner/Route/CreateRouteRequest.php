<?php

namespace App\Http\Requests\Owner\Route;

use App\Models\Route;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateRouteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can("create", Route::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "source_id" => "required|numeric|exists:hubs,id|unique:routes,source_id,NULL,id,destination_id," . $this->destination_id,
            "destination_id" => "required|numeric|exists:hubs,id"
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            "source_id.unique" => "The route already exists"
        ];
    }
}
