<?php

namespace App\Http\Requests\Owner\Route;

use App\Models\Route;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ListRoutesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('viewAny', Route::class);
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            "hubs" => "sometimes"
        ];
    }
}
