<?php

namespace App\Http\Requests\Owner\Parcel;

use App\Models\Parcel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can("update", $this->parcel);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "tracking_code" => "required|string|unique:parcels,tracking_code,".$this->parcel->id,
            "route_id" => "required|numeric|exists:routes,id",
            "orders" => "required|array",
            "orders.*" => "numeric|exists:orders,id"
        ];
    }
}
