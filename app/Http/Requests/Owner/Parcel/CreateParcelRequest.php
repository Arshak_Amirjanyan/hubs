<?php

namespace App\Http\Requests\Owner\Parcel;

use App\Models\Parcel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateParcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can("create", Parcel::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "tracking_code" => "required|string|unique:parcels,tracking_code",
            "route_id" => "required|numeric|exists:routes,id",
            "orders" => "required|array",
            "orders.*" => "numeric|exists:orders,id"
        ];
    }
}
