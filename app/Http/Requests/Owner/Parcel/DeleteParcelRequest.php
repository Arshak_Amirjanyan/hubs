<?php

namespace App\Http\Requests\Owner\Parcel;

use App\Http\Requests\DeleteModelRequest;
use App\Repositories\ParcelRepository;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Auth;

class DeleteParcelRequest extends DeleteModelRequest
{
    /**
     * @param ParcelRepository $repository
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param $content
     */
    public function __construct(ParcelRepository $repository, array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($repository, $query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can("delete", $this->parcel);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
