<?php

namespace App\Http\Requests\Owner\Order;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('create', Order::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "user_id" => 'required|numeric|exists:users,id',
            "tracking_code" => "sometimes",
            "start_location" => "array|required",
            "start_location.*" => "numeric|required",
            "end_location" => "array|required",
            "end_location.*" => "numeric|required",
            "delivery_status" => "required_unless:order_status,".Order::ORDER_STATUES["not_submitted"]."|in:" . implode(",", Order::DELIVERY_STATUSES),
            "order_status" => "required|in:" . implode(",", Order::ORDER_STATUES),
        ];
    }
}
