<?php

namespace App\Http\Requests\Owner\Order;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ListOrdersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('viewAny', [Order::class, $this]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "hubs" => "sometimes,exists:hubs,id",
            "user" => "sometimes|numeric|exists:users,id",
            "order_status" => "sometimes|string|in:".implode(",", Order::ORDER_STATUES),
            "delivery_status" => "sometimes|string|in:".implode(",", Order::DELIVERY_STATUSES),
            "pickup_type" => "sometimes|string|in:".implode(",", Order::DELIVERY_TYPES),
            "delivery_type" => "sometimes|string|in:".implode(",", Order::DELIVERY_TYPES),
        ];
    }

}
