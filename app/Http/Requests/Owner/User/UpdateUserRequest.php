<?php

namespace App\Http\Requests\Owner\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('update', [$this->user, $this]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'role_id' => 'required|numeric|exists:roles,id|in:'.implode("," , Auth::user()->roles()->first()->childRoles->pluck('id')->toArray()),
            'company_id' => 'required|exists:companies,id',
            'email' => 'required|unique:users,email,'.$this->user->id,
            'phone' =>'required|digits_between:8,12|numeric|unique:users,phone,'.$this->user->id,
            'password' => 'sometimes|min:6'
        ];
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            "role_id.exists:roles,id" => "Unable to create a user of this role",
            'phone.numeric' => "The phone must be numeric",
            'phone.digits_between:8,12' => "The phone must be between 8 and 12 characters",
            'password.min:6' => "The password must be at least 6 characters long",
        ];
    }
}
