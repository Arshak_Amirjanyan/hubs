<?php

namespace App\Http\Requests\Owner\User;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('create', [User::class, $this]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'company_id' =>'required|numeric|exists:companies,id',
            'role_id' => 'required|exists:roles,id|in:'.implode("," , Auth::user()->roles()->first()->childRoles->pluck("id")->toArray()),
            'email' => 'required|unique:users,email',
            'phone' =>'required|digits_between:8,12|numeric|unique:users,phone',
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            "role_id.exists:roles,id" => "Unable to create a user of this role",
            'phone.numeric' => "The phone must be numeric",
            'phone.digits_between:8,12' => "The phone must be between 8 and 12 characters",
            'password.min:6' => 'password must be at least 6 characters long'
        ];
    }
}
