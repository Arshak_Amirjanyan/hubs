<?php

namespace App\Http\Requests\Owner\Company;

use App\Http\Requests\DeleteModelRequest;
use App\Repositories\CompanyRepository;
use Illuminate\Support\Facades\Auth;

class DeleteCompanyRequest extends DeleteModelRequest
{
    /**
     * @param CompanyRepository $repository
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param $content
     */
    public function __construct(CompanyRepository $repository, array $query = [], array $request = [],
                                array $attributes = [], array $cookies = [], array $files = [],
                                array $server = [], $content = null)
    {
        parent::__construct($repository, $query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::user()->can('delete', $this->company);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
