<?php

namespace App\Http\Middleware;

use App\Http\Responses\ApiResponse;
use Closure;
use Illuminate\Http\Request;
use App\Exceptions\BadRequestException;

class VerifyJsonRequest
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws BadRequestException
     */
    public function handle(Request $request, Closure $next)
    {
        if (!($request->isMethod("get") || $request->isMethod("delete"))
            && $request->header('Content-Type') !== 'application/json') {
            throw new BadRequestException();
        }
        return $next($request);
    }
}
