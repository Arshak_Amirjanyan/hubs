<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyStatus
{
    /**
     * @param Request $request
     * @param Closure $next
     * @param string $status
     * @return mixed
     * @throws AuthorizationException
     */
    public function handle(Request $request, Closure $next, string $status)
    {
        if (Auth::check() && Auth::user()->company->status === $status) {
            return $next($request);
        }
        throw new AuthorizationException();
    }
}
