<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Partner\Permission\ListPermissionsRequest;
use App\Http\Requests\Partner\Permission\ShowPermissionRequest;
use App\Http\Resources\Permission\PermissionResource;
use App\Http\Resources\Permission\PermissionsCollection;
use App\Http\Responses\ApiResponse;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * @param ListPermissionsRequest $request
     * @return ApiResponse
     */
    public function index(ListPermissionsRequest $request): ApiResponse
    {
        return new ApiResponse(new PermissionsCollection($this->currentUser->getAllPermissions()));
    }

    /**
     * @param ShowPermissionRequest $request
     * @param Permission $permission
     * @return ApiResponse
     */
    public function show(ShowPermissionRequest $request, Permission $permission): ApiResponse
    {
        return new ApiResponse(PermissionResource::make($permission));
    }

}
