<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Partner\Order\CreateOrderRequest;
use App\Http\Requests\Partner\Order\DeleteOrderRequest;
use App\Http\Requests\Partner\Order\ListOrdersRequest;
use App\Http\Requests\Partner\Order\ShowOrderRequest;
use App\Http\Requests\Partner\Order\UpdateOrderRequest;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Order\OrdersCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Repositories\Repository;
use App\Services\OrderService;
use Facade\FlareClient\Api;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * @var Repository
     */
    protected Repository $orderRepository;

    /**
     * @var OrderService
     */
    protected OrderService $orderService;

    /**
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository, OrderService $orderService)
    {
        parent::__construct();
        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
    }

    /**
     * @param ListOrdersRequest $request
     * @return ApiResponse
     */
    public function index(ListOrdersRequest $request): ApiResponse
    {
        $filters = $this->orderService->filterPartnerOrders($request->validated());
        $orders = $this->orderRepository->applyFilters($filters)->get();

        return new ApiResponse(OrdersCollection::make($orders));
    }

    /**
     * @param ShowOrderRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function show(ShowOrderRequest $request, Order $order): ApiResponse
    {
        return new ApiResponse(OrderResource::make($order));
    }

    /**
     * @param CreateOrderRequest $request
     * @return ApiResponse
     */
    public function store(CreateOrderRequest $request): ApiResponse
    {
        $creationPayload = $this->orderService->preparePartnerCreationData($request->validated());
        $order = Order::create($creationPayload);

        return new ApiResponse(OrderResource::make($order));
    }

    /**
     * @param UpdateOrderRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function update(UpdateOrderRequest $request, Order $order): ApiResponse
    {
        $order->update($request->validated());

        return new ApiResponse(OrderResource::make($order));
    }

    /**
     * @param DeleteOrderRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function destroy(DeleteOrderRequest $request, Order $order): ApiResponse
    {
        $order->delete();

        return new ApiResponse(OrderResource::make($order));
    }
}
