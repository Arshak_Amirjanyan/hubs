<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Partner\Company\CreateCompanyRequest;
use App\Http\Requests\Partner\Company\DeleteCompanyRequest;
use App\Http\Requests\Partner\Company\ListCompaniesRequest;
use App\Http\Requests\Partner\Company\ShowCompanyRequest;
use App\Http\Requests\Partner\Company\UpdateCompanyRequest;
use App\Http\Resources\Company\CompaniesCollection;
use App\Http\Resources\Company\CompanyResource;
use App\Http\Responses\ApiResponse;
use App\Models\Company;

class CompanyController extends Controller
{
    /**
     * @param ListCompaniesRequest $request
     * @return ApiResponse
     */
    public function index(ListCompaniesRequest $request): ApiResponse
    {
        return new ApiResponse(CompaniesCollection::make(Company::all()));
    }

    /**
     * @param ShowCompanyRequest $request
     * @param Company $company
     * @return ApiResponse
     */
    public function show(ShowCompanyRequest $request, Company $company): ApiResponse
    {
        return new ApiResponse(CompanyResource::make($company));
    }

    /**
     * @param CreateCompanyRequest $request
     * @return ApiResponse
     */
    public function store(CreateCompanyRequest $request): ApiResponse
    {
        $company = Company::create(array_merge($request->validated(), ["status" => Company::PARTNER]));

        return new ApiResponse(CompanyResource::make($company));
    }

    /**
     * @param UpdateCompanyRequest $request
     * @param Company $company
     * @return ApiResponse
     */
    public function update(UpdateCompanyRequest $request, Company $company): ApiResponse
    {
        $company->update($request->validated());

        return new ApiResponse(CompanyResource::make($company));
    }

    /**
     * @param DeleteCompanyRequest $request
     * @param Company $company
     * @return ApiResponse
     */
    public function destroy(DeleteCompanyRequest $request, Company $company): ApiResponse
    {
        $company->delete();

        return new ApiResponse(CompanyResource::make($company));
    }
}
