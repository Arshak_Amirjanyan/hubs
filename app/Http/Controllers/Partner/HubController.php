<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Partner\Hub\ListHubsRequest;
use App\Http\Requests\Partner\Hub\ShowHubRequest;
use App\Http\Resources\Hub\HubResource;
use App\Http\Resources\Hub\HubsCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Hub;

class HubController extends Controller
{
    /**
     * @param ListHubsRequest $request
     * @return ApiResponse
     */
    public function index(ListHubsRequest $request): ApiResponse
    {
        return new ApiResponse(HubsCollection::make(Hub::all()));
    }

    /**
     * @param ShowHubRequest $request
     * @param Hub $hub
     * @return ApiResponse
     */
    public function show(ShowHubRequest $request, Hub $hub): ApiResponse
    {
        return new ApiResponse(HubResource::make($hub));
    }

}
