<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Role\ShowRoleRequest;
use App\Http\Requests\Partner\Role\ListRolesRequest;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Role\RolesCollection;
use App\Http\Responses\ApiResponse;
use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * @param ListRolesRequest $request
     * @return ApiResponse
     */
    public function index(ListRolesRequest $request): ApiResponse
    {
        $roles = $this->currentUser->roles()->first()
            ->childRoles()->with(['permissions', 'childRoles'])->get();

        return new ApiResponse(RolesCollection::make($roles));
    }

    /**
     * @param ShowRoleRequest $request
     * @param Role $role
     * @return ApiResponse
     */
    public function show(ShowRoleRequest $request, Role $role): ApiResponse
    {
        return new ApiResponse(RoleResource::make($role->load(['permissions', 'childRoles'])));
    }
}
