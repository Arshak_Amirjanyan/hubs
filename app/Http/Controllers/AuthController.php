<?php

namespace App\Http\Controllers;

use App\Exceptions\LoginException;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Http\Responses\ApiResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @return ApiResponse
     */
    public function register(RegisterRequest $request): ApiResponse
    {
        $user = User::create($request->validated());

        $role = Role::findByName('user');
        $user->assignRole($role->id);

        return new ApiResponse(UserResource::make($user), 200);
    }

    /**
     * @param LoginRequest $request
     * @return ApiResponse
     * @throws LoginException
     */
    public function login(LoginRequest $request): ApiResponse
    {
        $credentials = $request->only(['email', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            throw new LoginException();
        }

        return new ApiResponse(['token' => $token, 'user' => UserResource::make(Auth::user())]);
    }
}
