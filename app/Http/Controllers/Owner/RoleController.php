<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Role\CreateRoleRequest;
use App\Http\Requests\Owner\Role\DeleteRoleRequest;
use App\Http\Requests\Owner\Role\ListRolesRequest;
use App\Http\Requests\Owner\Role\ShowRoleRequest;
use App\Http\Requests\Owner\Role\UpdateRoleRequest;
use App\Http\Resources\Role\RoleResource;
use App\Http\Resources\Role\RolesCollection;
use App\Http\Responses\ApiResponse;
use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * @param ListRolesRequest $request
     * @return ApiResponse
     */
    public function index(ListRolesRequest $request): ApiResponse
    {
        $roles = $this->currentUser->roles()->first()
            ->childRoles()->with(['permissions', 'childRoles'])->get();
        return new ApiResponse(RolesCollection::make($roles));
    }

    /**
     * @param ShowRoleRequest $request
     * @param Role $role
     * @return ApiResponse
     */
    public function show(ShowRoleRequest $request, Role $role): ApiResponse
    {
        return new ApiResponse(RoleResource::make($role->load(['permissions', 'childRoles','parentRoles'])));
    }

    /**
     * @param CreateRoleRequest $request
     * @return ApiResponse
     */
    public function store(CreateRoleRequest $request): ApiResponse
    {
        $role = Role::create($request->validated());
        $role->syncPermissions($request->safe(['permissions']));
        $role->assignChildRoles($request->get('child_roles'));
        $role->assignParentRoles($request->get('parent_roles'));

        return new ApiResponse(RoleResource::make($role->load(['permissions', 'childRoles', 'parentRoles'])));
    }

    /**
     * @param UpdateRoleRequest $request
     * @param Role $role
     * @return ApiResponse
     */
    public function update(UpdateRoleRequest $request, Role $role): ApiResponse
    {
        $role->assignChildRoles($request->get('child_roles'), true);
        $role->assignParentRoles($request->get('parent_roles'), true);
        $role->update($request->validated());
        $role->syncPermissions($request->safe(['permissions']));

        return new ApiResponse(RoleResource::make($role->load(['permissions', 'childRoles', 'parentRoles'])));
    }

    /**
     * @param DeleteRoleRequest $request
     * @param Role $role
     * @return ApiResponse
     */
    public function destroy(DeleteRoleRequest $request, Role $role): ApiResponse
    {
        $role->delete();

        return new ApiResponse(RoleResource::make($role));
    }

}
