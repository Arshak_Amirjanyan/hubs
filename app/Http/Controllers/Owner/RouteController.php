<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Route\CreateRouteRequest;
use App\Http\Requests\Owner\Route\DeleteRouteRequest;
use App\Http\Requests\Owner\Route\ListRoutesRequest;
use App\Http\Requests\Owner\Route\ShowRouteRequest;
use App\Http\Requests\Owner\Route\UpdateRouteRequest;
use App\Http\Resources\Route\RouteResource;
use App\Http\Resources\Route\RoutesCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Route;

class RouteController extends Controller
{

    /**
     * @param ListRoutesRequest $request
     * @return ApiResponse
     */
    public function index(ListRoutesRequest $request): ApiResponse
    {
        return new ApiResponse(RoutesCollection::make(Route::all()));
    }

    /**
     * @param ShowRouteRequest $request
     * @param Route $route
     * @return ApiResponse
     */
    public function show(ShowRouteRequest $request, Route $route): ApiResponse
    {
        return new ApiResponse(RouteResource::make($route));
    }

    /**
     * @param CreateRouteRequest $request
     * @return ApiResponse
     */
    public function store(CreateRouteRequest $request): ApiResponse
    {
        $route = Route::Create($request->validated());

        return new ApiResponse(RouteResource::make($route));
    }

    /**
     * @param UpdateRouteRequest $request
     * @param Route $route
     * @return ApiResponse
     */
    public function update(UpdateRouteRequest $request, Route $route): ApiResponse
    {
        $route->update($request->validated());

        return new ApiResponse(RouteResource::make($route));
    }

    /**
     * @param DeleteRouteRequest $request
     * @param Route $route
     * @return ApiResponse
     */
    public function destroy(DeleteRouteRequest $request, Route $route): ApiResponse
    {
        $route->delete();

        return new ApiResponse(RouteResource::make($route));
    }
}
