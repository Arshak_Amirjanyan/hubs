<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Permission\ListPermissionsRequest;
use App\Http\Requests\Owner\Permission\ShowPermissionRequest;
use App\Http\Requests\Owner\User\ListUserPermissionsRequest;
use App\Http\Requests\Owner\User\UpdateUserPermissionRequest;
use App\Http\Requests\Owner\User\CreateUserRequest;
use App\Http\Requests\Owner\User\DeleteUserRequest;
use App\Http\Requests\Owner\User\ListUsersRequest;
use App\Http\Requests\Owner\User\ShowUserRequest;
use App\Http\Requests\Owner\User\UpdateUserRequest;
use App\Http\Resources\Permission\PermissionResource;
use App\Http\Resources\Permission\PermissionsCollection;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\UsersCollection;

use App\Http\Responses\ApiResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @param ListUsersRequest $request
     * @return ApiResponse
     */
    public function index(ListUsersRequest $request): ApiResponse
    {
        return new ApiResponse(new UsersCollection($this->currentUser->getVisibleUsersByRole()->get()));
    }

    /**
     * @param ShowUserRequest $request
     * @param User $user
     * @return ApiResponse
     */
    public function show(ShowUserRequest $request, User $user): ApiResponse
    {
        return new ApiResponse(UserResource::make($user));
    }

    /**
     * @param CreateUserRequest $request
     * @return ApiResponse
     */
    public function store(CreateUserRequest $request): ApiResponse
    {
        $user = User::create($request->safe()->except("role_id"));
        $user->assignRole($request->safe(["role_id"]));

        return new ApiResponse(UserResource::make($user));
    }

    /**
     * @param UpdateUserRequest $request
     * @param User $user
     * @return ApiResponse
     */
    public function update(UpdateUserRequest $request, User $user): ApiResponse
    {
        $user->update($request->safe()->except('role_id'));
        $user->syncRoles($request->safe(['role_id']));

        return new ApiResponse(UserResource::make($user));
    }

    /**
     * @param DeleteUserRequest $request
     * @param User $user
     * @return ApiResponse
     */
    public function destroy(DeleteUserRequest $request, User $user): ApiResponse
    {
        $user->delete();

        return new ApiResponse(UserResource::make($user));
    }


    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function getCurrentUser(Request $request): ApiResponse
    {
        $user = Auth::user();

        return new ApiResponse(UserResource::make($user));
    }

    /**
     * @param ListUserPermissionsRequest $request
     * @param User $user
     * @return ApiResponse
     */
    public function getUserPermissions(ListUserPermissionsRequest $request, User $user): ApiResponse
    {
        return new ApiResponse(["role" => PermissionsCollection::make($user->getPermissionsViaRoles()), "user" => $user->permissions]);
    }

    /**
     * @param UpdateUserPermissionRequest $request
     * @param User $user
     * @return ApiResponse
     */
    public function updateUserPermissions(UpdateUserPermissionRequest $request, User $user): ApiResponse
    {
        $user->syncPermissions($request->safe(["permissions"]));
        return new ApiResponse(PermissionsCollection::make($user->getAllPermissions()));
    }
}

