<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Permission\ListPermissionsRequest;
use App\Http\Requests\Owner\Permission\ShowPermissionRequest;
use App\Http\Resources\Permission\PermissionResource;
use App\Http\Resources\Permission\PermissionsCollection;
use App\Http\Responses\ApiResponse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * @param ListPermissionsRequest $request
     * @return ApiResponse
     */
    public function index(ListPermissionsRequest $request): ApiResponse
    {
        return new ApiResponse(new PermissionsCollection($this->currentUser->getAllPermissions()));
    }

    /**
     * @param ShowPermissionRequest $request
     * @param Permission $permission
     * @return ApiResponse
     */
    public function show(ShowPermissionRequest $request, Permission $permission): ApiResponse
    {
        return new ApiResponse(PermissionResource::make($permission));
    }

}
