<?php

namespace App\Http\Controllers\Owner;

use App\Events\RouteSynced;
use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Order\AssignRouteRequest;
use App\Http\Requests\Owner\Order\CreateOrderRequest;
use App\Http\Requests\Owner\Order\DeleteOrderRequest;
use App\Http\Requests\Owner\Order\GenerateRouteRequest;
use App\Http\Requests\Owner\Order\ListOrdersRequest;
use App\Http\Requests\Owner\Order\ScanRouteRequest;
use App\Http\Requests\Owner\Order\ShowOrderRequest;
use App\Http\Requests\Owner\Order\UpdateOrderRequest;
use App\Http\Resources\GeneratedPaths\GeneratedPathsCollection;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Order\OrdersCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Hub;
use App\Models\Order;
use App\Models\Route;
use App\Repositories\OrderRepository;
use App\Repositories\Repository;
use App\Services\DeliveryService;
use App\Services\OrderService;
use App\Services\RouteService;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * @var Repository
     */
    protected Repository $orderRepository;

    /**
     * @var OrderService
     */
    protected OrderService $orderService;

    /**
     * @var RouteService
     */
    protected RouteService $routeService;

    /**
     * @var DeliveryService
     */
    protected DeliveryService $deliveryService;

    /**
     * @param OrderRepository $orderRepository
     * @param RouteService $routeService
     * @param OrderService $orderService
     * @param DeliveryService $deliveryService
     */
    public function __construct(OrderRepository $orderRepository, RouteService $routeService,
                                OrderService    $orderService, DeliveryService $deliveryService)
    {
        parent::__construct();
        $this->orderRepository = $orderRepository;
        $this->routeService = $routeService;
        $this->orderService = $orderService;
        $this->deliveryService = $deliveryService;
    }

    /**
     * @param ListOrdersRequest $request
     * @return ApiResponse
     */
    public function index(ListOrdersRequest $request): ApiResponse
    {
        DB::enableQueryLog();
        $orders = $this->orderRepository->applyFilters($request->validated())->
        with(["sourceHub", "destinationHub"])->get();

        return new ApiResponse(OrdersCollection::make($orders));
    }

    /**
     * @param ShowOrderRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function show(ShowOrderRequest $request, Order $order): ApiResponse
    {
        return new ApiResponse(OrderResource::make($order->load("user", "routes", "sourceHub", "destinationHub")));
    }

    /**
     * @param CreateOrderRequest $request
     * @return ApiResponse
     */
    public function store(CreateOrderRequest $request): ApiResponse
    {
        $data = $this->orderService->prepareOwnerCreationData($request->validated());
        $order = Order::create($data);

        return new ApiResponse(OrderResource::make($order->load(["sourceHub", "destinationHub"])));
    }

    /**
     * @param UpdateOrderRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function update(UpdateOrderRequest $request, Order $order): ApiResponse
    {
        $order->update($request->validated());

        return new ApiResponse(OrderResource::make($order->load(["sourceHub", "destinationHub"])));
    }

    /**
     * @param DeleteOrderRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function destroy(DeleteOrderRequest $request, Order $order): ApiResponse
    {
        $order->delete();

        return new ApiResponse(OrderResource::make($order));
    }

    /**
     * @param GenerateRouteRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function generateRoute(GenerateRouteRequest $request, Order $order): ApiResponse
    {
        $routesAdjacencyList = Hub::with('routesFrom', 'routesFrom.sourceHub', 'routesFrom.destinationHub')->get("id")->pluck("routesFrom", "id");
        $generatedRoutes = $this->routeService->generateRoutes(Hub::find($order->source_id), Hub::find($order->destination_id), $routesAdjacencyList);

        return new ApiResponse(GeneratedPathsCollection::make($generatedRoutes));
    }

    /**
     * @param AssignRouteRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function assignRoute(AssignRouteRequest $request, Order $order): ApiResponse
    {
        $changedRoutes = $order->routes()->sync($request->get("path"));
        RouteSynced::dispatch($changedRoutes);

        return new ApiResponse(OrderResource::make($order->load("routes")));
    }

    /**
     * @param ScanRouteRequest $request
     * @param Order $order
     * @return ApiResponse
     */
    public function scan(ScanRouteRequest $request, Order $order): ApiResponse
    {
        $order->update(["current_location_id" => $request->get("hub_id"),
            "delivery_status" => $this->deliveryService->getNextDeliveryStatus($order->delivery_status ?? ""),
            "order_status" => Order::ORDER_STATUES["submitted"]]);
        $this->orderService->updateRouteStatus($order, $request->get("hub_id"));

        return new ApiResponse(OrderResource::make($order->load("user", "routes", "sourceHub", "destinationHub")));
    }

}
