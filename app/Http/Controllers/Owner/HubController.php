<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Hub\DeleteHubRequest;
use App\Http\Requests\Owner\Hub\CreateHubRequest;
use App\Http\Requests\Owner\Hub\ListHubsRequest;
use App\Http\Requests\Owner\Hub\ShowHubRequest;
use App\Http\Requests\Owner\Hub\UpdateHubRequest;
use App\Http\Resources\Hub\HubResource;
use App\Http\Resources\Hub\HubsCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Hub;
use Illuminate\Http\Request;

class HubController extends Controller
{
    /**
     * @param ListHubsRequest $request
     * @return ApiResponse
     */
    public function index(ListHubsRequest $request): ApiResponse
    {
        return new ApiResponse(HubsCollection::make(Hub::all()));
    }

    /**
     * @param ShowHubRequest $request
     * @param Hub $hub
     * @return ApiResponse
     */
    public function show(ShowHubRequest $request, Hub $hub): ApiResponse
    {
        return new ApiResponse(HubResource::make($hub));
    }

    /**
     * @param CreateHubRequest $request
     * @return ApiResponse
     */
    public function store(CreateHubRequest $request): ApiResponse
    {
        $hub = Hub::create($request->validated());

        return new ApiResponse(HubResource::make($hub));
    }

    /**
     * @param UpdateHubRequest $request
     * @param Hub $hub
     * @return ApiResponse
     */
    public function update(UpdateHubRequest $request, Hub $hub): ApiResponse
    {
        $hub->update($request->validated());

        return new ApiResponse(HubResource::make($hub));
    }

    /**
     * @param DeleteHubRequest $request
     * @param Hub $hub
     * @return ApiResponse
     */
    public function destroy(DeleteHubRequest $request, Hub $hub): ApiResponse
    {
        $hub->delete();

        return new ApiResponse(HubResource::make($hub));
    }
}
