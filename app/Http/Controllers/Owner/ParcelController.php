<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Parcel\CreateParcelRequest;
use App\Http\Requests\Owner\Parcel\DeleteParcelRequest;
use App\Http\Requests\Owner\Parcel\ListParcelsRequest;
use App\Http\Requests\Owner\Parcel\ShowParcelRequest;
use App\Http\Requests\Owner\Parcel\UpdateParcelRequest;
use App\Http\Resources\Parcel\ParcelResource;
use App\Http\Resources\Parcel\ParcelsCollection;
use App\Http\Responses\ApiResponse;
use App\Models\Parcel;
use App\Repositories\ParcelRepository;
use App\Repositories\Repository;

class ParcelController extends Controller
{

    /**
     * @var Repository
     */
    protected Repository $parcelRepository;

    /**
     * @param ParcelRepository $parcelRepository
     */
    public function __construct(ParcelRepository $parcelRepository)
    {
        parent::__construct();
        $this->parcelRepository = $parcelRepository;
    }

    /**
     * @param ListParcelsRequest $request
     * @return ApiResponse
     */
    public function index(ListParcelsRequest $request): ApiResponse
    {
        $parcels = $this->parcelRepository->applyFilters($request->validated())->get();

        return new ApiResponse(ParcelsCollection::make($parcels));
    }

    /**
     * @param ShowParcelRequest $request
     * @param Parcel $parcel
     * @return ApiResponse
     */
    public function show(ShowParcelRequest $request, Parcel $parcel): ApiResponse
    {
        return new ApiResponse(ParcelResource::make($parcel->load("orders")));
    }

    /**
     * @param CreateParcelRequest $request
     * @return ApiResponse
     */
    public function store(CreateParcelRequest $request): ApiResponse
    {
        $parcel = Parcel::create($request->validated());
        $this->parcelRepository->addOrders($parcel, $request->get("orders"));

        return new ApiResponse(ParcelResource::make($parcel));
    }

    /**
     * @param UpdateParcelRequest $request
     * @param Parcel $parcel
     * @return ApiResponse
     */
    public function update(UpdateParcelRequest $request, Parcel $parcel): ApiResponse
    {
        $parcel->update($request->validated());

        return new ApiResponse(ParcelResource::make($parcel));
    }

    /**
     * @param DeleteParcelRequest $request
     * @param Parcel $parcel
     * @return ApiResponse
     */
    public function destroy(DeleteParcelRequest $request, Parcel $parcel): ApiResponse
    {
        $parcel->delete();

        return new ApiResponse(ParcelResource::make($parcel));
    }
}
