<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            [
                'name' => 'Hub system',
                'status' => Company::STATUSES["owner"],
            ],
            [
                'name' => 'Vazgennoc',
                'status' => Company::STATUSES["partner"],
            ]
        ];

        foreach ($companies as $company) {
            Company::create($company);
        }
    }
}
