<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /*
     * Available Roles
     */
    protected const SUPERADMIN = 'superadmin';
    protected const ADMIN = 'admin';
    protected const MANAGER = 'manager';
    protected const COURIER = 'courier';
    protected const PARTNER_ADMIN = 'partner_admin';
    protected const PARTNER_MANAGER = 'partner_manager';
    protected const PARTNER_COURIER = 'partner_courier';
    protected const USER = 'user';

    /**
     * all permissions
     */
    protected const PERMISSIONS = [
        'listAllUsers' => 'user-full',
        'listUsers' => 'user-list',
        'viewUser' => 'user-view',
        'addUser' => 'user-create',
        'editUser' => 'user-edit',
        'deleteUser' => 'user-delete',
        'listRoles' => 'role-list',
        'viewRoles' => 'role-view',
        'addRole' => 'role-create',
        'editRole' => 'role-edit',
        'deleteRole' => 'role-delete',
        'listPermissions' => 'permission-list',
        'viewPermission' => 'permission-view',
        'editPermission' => 'permission-update',
        'listAllHubs' => 'hub-full',
        'listHubs' => 'hub-list',
        'viewHub' => 'hub-view',
        'addHub' => 'hub-create',
        'editHub' => 'hub-edit',
        'deleteHub' => 'hub-delete',
        'listCompanies' => 'company-list',
        'viewCompany' => 'company-view',
        'addCompany' => 'company-create',
        'editCompany' => 'company-edit',
        'deleteCompany' => 'company-delete',
        'listAllOrders' => 'order-full',
        'listOrders' => 'order-list',
        'viewOrder' => 'order-view',
        'addOrder' => 'order-create',
        'editOrder' => 'order-edit',
        'deleteOrder' => 'order-delete',
        'generateRoute' => 'route-generate',
        'assignRoute' => 'route-assign',
        'listAllParcels' => 'parcel-full',
        'listParcels' => 'parcel-list',
        'viewParcel' => 'parcel-view',
        'addParcel' => 'parcel-create',
        'editParcel' => 'parcel-edit',
        'deleteParcel' => 'parcel-delete',
        'listAllRoutes' => 'route-full',
        'listRoutes' => 'route-list',
        'viewRoute' => 'route-view',
        'addRoute' => 'route-create',
        'editRoute' => 'route-edit',
        'deleteRoute' => 'route-delete'
    ];

    /**
     * permissions by role
     */
    protected const ROLE_PERMISSIONS = [

        /*
         * superadmin permissions
         */
        'superadmin' => [self::PERMISSIONS],

        /*
         * admin permissions
         */
        'admin' => [
            self::PERMISSIONS['viewUser'],
            self::PERMISSIONS['listUsers'],
            self::PERMISSIONS['addUser'],
            self::PERMISSIONS['editUser'],
            self::PERMISSIONS['deleteUser'],
            self::PERMISSIONS['listRoles'],
            self::PERMISSIONS['viewRoles'],
            self::PERMISSIONS['listPermissions'],
            self::PERMISSIONS['viewPermission'],
            self::PERMISSIONS['listHubs'],
            self::PERMISSIONS['viewHub'],
            self::PERMISSIONS['listCompanies'],
            self::PERMISSIONS['viewCompany'],
            self::PERMISSIONS['addCompany'],
            self::PERMISSIONS['editCompany'],
            self::PERMISSIONS['deleteCompany'],
            self::PERMISSIONS['listOrders'],
            self::PERMISSIONS['viewOrder'],
            self::PERMISSIONS['addOrder'],
            self::PERMISSIONS['editOrder'],
            self::PERMISSIONS['deleteOrder'],
            self::PERMISSIONS['listAllRoutes'],
        ],
        /*
         * manager permissions
         */
        'manager' => [
            self::PERMISSIONS['viewUser'],
            self::PERMISSIONS['listUsers'],
            self::PERMISSIONS['addUser'],
            self::PERMISSIONS['editUser'],
            self::PERMISSIONS['deleteUser'],
            self::PERMISSIONS['listRoles'],
            self::PERMISSIONS['viewRoles'],
            self::PERMISSIONS['listPermissions'],
            self::PERMISSIONS['listHubs'],
            self::PERMISSIONS['viewHub'],
            self::PERMISSIONS['listCompanies'],
            self::PERMISSIONS['viewCompany'],
            self::PERMISSIONS['addCompany'],
            self::PERMISSIONS['editCompany'],
            self::PERMISSIONS['deleteCompany'],
            self::PERMISSIONS['listOrders'],
            self::PERMISSIONS['viewOrder'],
            self::PERMISSIONS['addOrder'],
            self::PERMISSIONS['editOrder'],
            self::PERMISSIONS['deleteOrder'],
            self::PERMISSIONS['listRoutes'],
            self::PERMISSIONS['viewRoute'],

        ],

        /*
         * courier permissions
         */
        'courier' => [],

        /**
         * partner admin permissions
         */
        'partner_admin' => [
            self::PERMISSIONS['viewUser'],
            self::PERMISSIONS['listUsers'],
            self::PERMISSIONS['addUser'],
            self::PERMISSIONS['editUser'],
            self::PERMISSIONS['deleteUser'],
            self::PERMISSIONS['listRoles'],
            self::PERMISSIONS['viewRoles'],
            self::PERMISSIONS['listPermissions'],
            self::PERMISSIONS['viewPermission'],
            self::PERMISSIONS['listHubs'],
            self::PERMISSIONS['viewHub'],
            self::PERMISSIONS['viewCompany'],
            self::PERMISSIONS['listOrders'],
            self::PERMISSIONS['viewOrder'],
            self::PERMISSIONS['addOrder'],
            self::PERMISSIONS['editOrder'],
            self::PERMISSIONS['deleteOrder'],
        ],

        /*
         * partner manager permissions
         */
        'partner_manager' => [
            self::PERMISSIONS['viewUser'],
            self::PERMISSIONS['listUsers'],
            self::PERMISSIONS['listRoles'],
            self::PERMISSIONS['viewRoles'],
            self::PERMISSIONS['listPermissions'],
            self::PERMISSIONS['listHubs'],
            self::PERMISSIONS['viewHub'],
            self::PERMISSIONS['listOrders'],
            self::PERMISSIONS['viewOrder'],
            self::PERMISSIONS['addOrder'],
            self::PERMISSIONS['editOrder'],
            self::PERMISSIONS['deleteOrder']
        ],

        /*
         * partner courier permissions
         */
        'partner_courier' => [
            self::PERMISSIONS['viewUser'],
            self::PERMISSIONS['listUsers'],
            self::PERMISSIONS['listRoles'],
            self::PERMISSIONS['viewRoles'],
            self::PERMISSIONS['listPermissions'],
            self::PERMISSIONS['listHubs'],
            self::PERMISSIONS['viewHub'],
            self::PERMISSIONS['listOrders'],
            self::PERMISSIONS['viewOrder']
        ],

        /*
        * user permissions
        */
        'user' => [],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::PERMISSIONS as $permission) {
            Permission::updateOrCreate(['name' => $permission]);
        }

        foreach (self::ROLE_PERMISSIONS as $role => $permissions) {
            $createdRole = Role::updateOrCreate(['name' => $role, 'guard_name' => 'api']);
            foreach ($permissions as $permission) {
                $createdRole->givePermissionTo($permission);
            }
        }

        $superAdminRole = Role::findByName(self::SUPERADMIN);
        $adminRole = Role::findByName(self::ADMIN);
        $managerRole = Role::findByName(self::MANAGER);
        $courierRole = Role::findByName(self::COURIER);
        $partnerAdminRole = Role::findByName(self::PARTNER_ADMIN);
        $partnerManagerRole = Role::findByName(self::PARTNER_MANAGER);
        $partnerCourierRole = Role::findByName(self::PARTNER_COURIER);
        $userRole = Role::findByName(self::USER);

        //     $superAdminRole->assignChildRoles([$superAdminRole->id, $partnerRole->id, $managerRole->id, $courierRole->id, $userRole->id]);
        //     $partnerRole->assignableRoles()->sync([$managerRole->id, $courierRole->id, $userRole->id]);
        $adminRole->assignChildRoles([$managerRole->id, $courierRole->id, $partnerAdminRole->id, $partnerManagerRole->id, $partnerCourierRole->id, $userRole->id]);
        $managerRole->assignChildRoles([$courierRole->id, $partnerAdminRole->id, $partnerManagerRole->id, $partnerCourierRole->id, $userRole->id]);
        $courierRole->assignChildRoles([$userRole->id]);
        $partnerAdminRole->assignChildRoles([$partnerAdminRole->id, $partnerManagerRole->id, $partnerCourierRole->id, $userRole->id]);
        $partnerCourierRole->assignChildRoles($userRole->id);
    }
}
