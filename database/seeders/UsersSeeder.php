<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminData = [
            'company_id' => 1,
            'name' => 'superadmin',
            'email' => 'super@admin.com',
            'phone' => 37441644373,
            'password' => 'super'
        ];

        $adminRole = Role::where('name', 'superadmin')->first();
        $admin = User::create($adminData);
        $admin->assignRole($adminRole->id);

        $userRole = Role::where('name', 'user')->first();
        $users = User::factory(5)->create()->each(function ($user) use ($userRole) {
            $user->assignRole($userRole->id);
            $user->save();
        });
    }
}
