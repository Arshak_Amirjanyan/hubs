<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger("source_id");
            $table->unsignedBigInteger("destination_id");
            $table->unsignedBigInteger("current_location_id")->nullable();
            $table->string("tracking_code");
            $table->json("start_location");
            $table->json("end_location");
            $table->string("delivery_status")->nullable();
            $table->string("order_status");
            $table->string("pickup_type");
            $table->string("delivery_type");
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("source_id")->references("id")->on("hubs");
            $table->foreign("current_location_id")->references("id")->on("hubs");
            $table->foreign("destination_id")->references("id")->on("hubs");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
