<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_routes', function (Blueprint $table) {
          $table->unsignedBigInteger("order_id");
          $table->unsignedBigInteger("route_id");
          $table->string("status");

            $table->foreign('order_id')
                ->references('id')
                ->on('orders')->onDelete("cascade");
            $table->foreign('route_id')
                ->references('id')
                ->on('routes')->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_routes');
    }
}
