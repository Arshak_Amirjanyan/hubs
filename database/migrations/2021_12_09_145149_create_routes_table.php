<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("source_id");
            $table->unsignedBigInteger("destination_id");
            $table->integer("current_order_count")->default(0);
            $table->timestamps();

            $table->foreign("source_id")->references('id')->on("hubs")->onDelete('cascade');
            $table->foreign("destination_id")->references('id')->on("hubs")->onDelete('cascade');
            $table->unique(['source_id', 'destination_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
