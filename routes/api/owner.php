<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('', 'UserController@index');
        Route::post('', 'UserController@store');
        Route::get('/current', 'UserController@getCurrentUser');
        Route::get('/{user}', 'UserController@show');
        Route::put('/{user}', 'UserController@update');
        Route::delete('/{user}', 'UserController@destroy');
        Route::get('/{user}/permissions', 'UserController@getUserPermissions');
        Route::put('/{user}/permissions', 'UserController@updateUserPermissions');
    });
    Route::prefix('roles')->group(function () {
        Route::get('', 'RoleController@index');
        Route::post('', 'RoleController@store');
        Route::get('/{role}', 'RoleController@show');
        Route::put('/{role}', 'RoleController@update');
        Route::delete('/{role}', 'RoleController@destroy');
    });
    Route::prefix('permissions')->group(function () {
        Route::get('', 'PermissionController@index');
        Route::get('/{permission}', 'PermissionController@show');
    });
    Route::prefix('hubs')->group(function () {
        Route::get('', 'HubController@index');
        Route::post('', 'HubController@store');
        Route::get('/{hub}', 'HubController@show');
        Route::put('/{hub}', 'HubController@update');
        Route::delete('/{hub}', 'HubController@destroy');
    });
    Route::prefix('companies')->group(function () {
        Route::get('', 'CompanyController@index');
        Route::post('', 'CompanyController@store');
        Route::get('/{company}', 'CompanyController@show');
        Route::put('/{company}', 'CompanyController@update');
        Route::delete('/{company}', 'CompanyController@destroy');
    });
    Route::prefix('orders')->group(function () {
        Route::get('', 'OrderController@index');
        Route::post('', 'OrderController@store');
        Route::get('/{order}', 'OrderController@show');
        Route::put('/{order}', 'OrderController@update');
        Route::delete('/{order}', 'OrderController@destroy');
        Route::get('/{order}/generate-route', 'OrderController@generateRoute');
        Route::post('/{order}/assign-route', 'OrderController@assignRoute');
        Route::post('/{order}/scan', 'OrderController@scan');
    });
    Route::prefix('parcels')->group(function () {
        Route::get('', 'ParcelController@index');
        Route::post('', 'ParcelController@store');
        Route::get('/{parcel}', 'ParcelController@show');
        Route::put('/{parcel}', 'ParcelController@update');
        Route::delete('/{parcel}', 'ParcelController@destroy');
    });
    Route::prefix('routes')->group(function () {
        Route::get('', 'RouteController@index');
        Route::post('', 'RouteController@store');
        Route::get('/{route}', 'RouteController@show');
        Route::put('/{route}', 'RouteController@update');
        Route::delete('/{route}', 'RouteController@destroy');
    });
});



