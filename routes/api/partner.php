<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('', 'UserController@index');
        Route::post('', 'UserController@store');
        Route::get('/{user}', 'UserController@show');
        Route::put('/{user}', 'UserController@update');
        Route::delete('/{user}', 'UserController@destroy');
        Route::get('/{user}/permissions', 'UserController@getUserPermissions');
        Route::put('/{user}/permissions', 'UserController@updateUserPermissions');
    });
    Route::prefix('roles')->group(function () {
        Route::get('', 'RoleController@index');
        Route::get('/{role}', 'RoleController@show');
    });
    Route::prefix('permissions')->group(function () {
        Route::get('', 'PermissionController@index');
        Route::get('/{permission}', 'PermissionController@show');
    });
    Route::prefix('companies')->group(function () {
        Route::get('', 'CompanyController@index');
        Route::post('', 'CompanyController@store');
        Route::get('/{company}', 'CompanyController@show');
        Route::put('/{company}', 'CompanyController@update');
        Route::delete('/{company}', 'CompanyController@destroy');
    });
    Route::prefix('hubs')->group(function () {
        Route::get('', 'HubController@index');
        Route::get('/{hub}', 'HubController@show');
    });
    Route::prefix('orders')->group(function () {
        Route::get('', 'OrderController@index');
        Route::get('/{order}', 'OrderController@show');
        Route::post('', 'OrderController@store');
        Route::put('/{order}', 'OrderController@update');
        Route::delete('/{order}', 'OrderController@destroy');
    });
});



